from __future__ import division
import math
import operator as op

#convert a string of characters into a list of tokens
# def tokenize(program):
#     tokens = []
#     index = 0
#     while index < len(program):
#         if program[index].isalpha():
#             symbol_index = index #index of the symbol start as first letter
#             tokens.append(program[symbol_index]) #append the first letter
#             current_token = len(tokens)-1 #current token is len-1
#             while program[symbol_index+1].isalpha(): #continue after the first letter
#                 tokens[current_token] += program[symbol_index+1] #append letter to the current token
#                 symbol_index+=1 #go to next letter
#             index = symbol_index #skip to end of the symbol in the program
#         elif program[index].isdigit():
#             digit_index = index #index of digit start
#             tokens.append(program[digit_index]) #append first digit
#             current_token = len(tokens)-1 #current token index
#             while program[digit_index+1].isdigit():
#                 tokens[current_token] += program[digit_index+1] #append the digit to the current token
#                 digit_index+=1 #go to next digit
#             index = digit_index #skip allready seen digits
#         elif program[index] != " ": #append anything that is not white space
#             tokens.append(program[index])
#         index+=1 #go to next character
#     return tokens

def tokenize(program):
    return program.replace("(", " ( ").replace(")", " ) ").split()

#convert list of tokens into abstract syntax tree
def read_from_tokens(tokens):
    if len(tokens) == 0:
        raise SyntaxError("unexpected EOF")
    token = tokens.pop(0) #the first element of tokens
    #begin process if first element is opening paren
    if token == '(':
        sub_list = []
        #check if 2nd element is not closing paren
        while tokens[0] != ')':
            #recursivley add to sub_list
            sub_list.append(read_from_tokens(tokens))
        tokens.pop(0)
        return sub_list
    #check if invalid paren
    elif token == ')':
        raise SyntaxError("unexpected ')'")
    else:
        #return the value of the token
        return num_or_symbol(token)

#give tokens number or symbol values
def num_or_symbol(token):
    try:
        return int(token)
    except ValueError:
        try:
            return float(token)
        except ValueError:
            return str(token)

#defining scheme standard prodecures
def standard_env():
   env = Env() 
   env.update(vars(math)) #sin, cos, sqrt, pi
   env.update({
       '+':             op.add,
       '-':             op.sub,
       '*':             op.mul,
       '/':             op.truediv,
       '>':             op.gt,
       '<':             op.lt,
       '>=':            op.ge,
       '<=':            op.le,
       '=':             op.eq,
       'abs':           abs,
       'append':        op.add,
       'apply':         lambda proc, args: proc(*args),
       'begin':         lambda *x: x[-1],
       'car':           lambda x: x[0],
       'cdr':           lambda x: x[1:],
       'cons':          lambda x,y: [x] + y,
       'eq?':           op.is_,
       'expt':          pow,
       'equal?':        op.eq,
       'length':        len,
       'list':          lambda *x: List(x),
       'list?':         lambda x: isinstance(x, List),
       'map':           map,
       'max':           max,
       'min':           min,
       'not':           op.not_,
       'null?':         lambda x: x == [],
       'number':        lambda x: isinstance(x, Number),
       'print':         print,
       'procedure?':    callable,
       'round':         round,
       'symbol?':       lambda x:isinstance(x, Symbol),
   })
   return env

global_env = standard_env()

def eval(x, evn = global_env):
    if isinstance(x, Symbol):
        return env[x]
    elif isinstance(x, Number):
        return x
    elif x[0] == 'if':
        (_, test, conseq, alt) = x
        exp = (conseq if eval(test, env) else alt)
    elif x[0] == 'define':
        (_, symbol, exp) = x
        env[symbol] = eval(exp, env)
    else:
        proc = eval(x[0], env)
        args = [eval(arg, env) for arg in x[1:]]
        return proc(*args)

def parse(program):
    read_from_tokens(tokenize(program))

program = "(begin (define r 10) (* pi (* r r)))" 
print(f"program: {program}")
parse(program)
